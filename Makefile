CC=mpicxx
CFLAGS=-std=c++1y -Wall -O3 -pthread

TARGET ?=
LIB_PATH ?= $(HOME)/tasks/pp_task

INC = $(LIB_PATH)/include

FILTR_OUT :=

MODULES  :=
SRCDIR := $(LIB_PATH)/src
BUILDDIR := $(LIB_PATH)/build/
BUILDDIRS := $(addprefix $(BUILDDIR),$(MODULES))

SRC=$(wildcard $(SRCDIR)/*/*.cpp) $(wildcard $(SRCDIR)/*.cpp)
SRCFILES:=$(filter-out $(FILTR_OUT), $(SRC))
OBJ=$(SRCFILES:$(SRCDIR)/%.cpp=$(BUILDDIR)/%.o)

OUT=proc_manager
LIB=balance

all: program

#dir: $(BUILDDIRS) TODO: uncomment when $MODULES become not empty
dir:
	@mkdir -p $(BUILDDIR)

$(OBJ): $(BUILDDIR)/%.o : $(SRCDIR)/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@ -I $(INC)

$(BUILDDIRS):
	@mkdir -p $@

lib$(LIB).a: dir $(OBJ)
	@ar r lib$(LIB).a $(OBJ)

program: lib$(LIB).a
	$(CC) $(CFLAGS) -c $(TARGET).cpp -I $(INC)
	$(CC) $(CFLAGS) -o $(OUT) $(TARGET).o -L ./ -l$(LIB)

clean:
	rm -fr $(OUT) $(BUILDDIR) $(TARGET).o lib$(LIB).a

