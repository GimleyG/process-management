#pragma once

#include <memory>

namespace dyn_proc {

class base_proxy_ptr : public std::enable_shared_from_this<base_proxy_ptr> {
public:
    virtual ~base_proxy_ptr() {}
};

template <class T>
class inheritable_enable_shared_from_this : virtual public base_proxy_ptr
{
public:
    std::shared_ptr<T> shared_from_this() {
        return std::dynamic_pointer_cast<T>(base_proxy_ptr::shared_from_this());
    }
    /* Utility method to easily downcast.
    * Useful when a child doesn't inherit directly from enable_shared_from_this
    * but wants to use the feature.
    */
    template <class Down>
    std::shared_ptr<Down> downcasted_shared_from_this() {
        return std::dynamic_pointer_cast<Down>(base_proxy_ptr::shared_from_this());
    }
};

}
