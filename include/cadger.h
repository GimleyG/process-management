#pragma once

#include <iostream>

#include "mpi.h"

#include "comm_types.h"
#include "i_tsk_queue.h"
#include "i_neigh_cont.h"
#include "job.h"

#include "procinfo.h"

namespace dyn_proc {

enum class CadgerRunMode
{
    DEFAULT,
    CHNG_COMM
};

/**
* A class that is used to ask jobs from neighbor processes and fill up the task queue
*/
class Cadger {
public:
    Cadger() = delete;
    Cadger(tsk_queue_ptr ptr, neigh_cont_ptr neigh)
        : tsk_queue_{ptr}, neigh_cont_{neigh} {}

    void run(CadgerRunMode mode, MPI_Comm comm);

private:
    tsk_queue_ptr tsk_queue_;
    neigh_cont_ptr neigh_cont_;
};

}
