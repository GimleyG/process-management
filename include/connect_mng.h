#pragma once

#include "mpi.h"
#include "types.h"
#include "i_comm_keeper.h"

#include <string>
#include <vector>

const int STATUS_OK = 200;
const int STATUS_QUIT = 201;


namespace dyn_proc {



/**
  @brief The ConnectManager class
  This class manage connection between different groups of mpi-processes
 */
class ConnectManager {
public:
    ConnectManager() : glob_comm_(MPI_COMM_WORLD) {}
    ConnectManager(Role role, MPI_Comm &glob_comm) : role_(role), glob_comm_(glob_comm) {}
    ~ConnectManager();

    void start();
    void stop();

    void addCommKeeper(CommKeeperPtr ptr);

private:
    Role role_;
    MPI_Comm intercomm_;
    MPI_Comm glob_comm_;
    std::string port_name_;
    int high_;
    std::thread *thr;

    std::vector<CommKeeperPtr> comm_keepers_;

    void run();
};

}
