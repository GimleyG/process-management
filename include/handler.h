#pragma once

#include <condition_variable>
#include <iostream>
#include <mutex>
#include "mpi.h"

#include "comm_types.h"
#include "i_comm_keeper.h"
#include "i_tsk_queue.h"
#include "types.h"

namespace dyn_proc {


/**
* A class that accepts incoming job request and sends job back if there is any in the task queue, 
* otherwise sends an empty job
*/
class Handler : public ICommKeeper {
public:
    Handler(tsk_queue_ptr tsk_queue, MPI_Comm comm, int active_neigh)
        : tsk_queue_{tsk_queue}, comm_{ comm }, active_neighbor_count_{active_neigh} {}

    virtual ~Handler() = default;

    void run();
    void terminate();

    // ICommKeeper
    virtual void changeComm(MPI_Comm comm);

private:
    tsk_queue_ptr tsk_queue_;
    MPI_Comm comm_;
    bool finished_;
    int active_neighbor_count_;

    std::mutex mtx_, active_neigh_mtx_;
    std::condition_variable active_neigh_cond_;
};

}
