#pragma once

#include <memory>
#include "mpi.h"


namespace dyn_proc {

/*
* An interface of mpi communicator keeper. 
* Every class that uses mpi communicator must be derived from this interface
*/
class ICommKeeper {
public:
    virtual ~ICommKeeper() = default;

    virtual void changeComm(MPI_Comm new_comm) = 0;
};


typedef std::shared_ptr<ICommKeeper> CommKeeperPtr;
}
