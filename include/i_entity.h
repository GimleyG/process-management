#pragma once

namespace dyn_proc {

/**
* A base interface for all entities
*/
class IEntity {
public:
    virtual ~IEntity() {}

    virtual bool isNone() = 0;
};

}
