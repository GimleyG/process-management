#pragma once

#include <memory>
#include "i_entity.h"

namespace dyn_proc {

/**
* A base interface for a job
*/
class IJob : public IEntity {
public:
    virtual ~IJob() {}
    virtual void perform() = 0;
    virtual int getData() = 0;
};

typedef std::shared_ptr<IJob> job_ptr;

job_ptr createJob(int weight);
}
