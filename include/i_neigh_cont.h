#pragma once

#include "base_proxy_ptr.h"
#include "i_neighbor.h"

namespace dyn_proc {
/**
* A base interface for a container that stores neighbor's information
*/
class INeighborsContainer : public inheritable_enable_shared_from_this<INeighborsContainer> {
public:
    virtual ~INeighborsContainer() {}

    virtual neigh_ptr getNeighbor() = 0;
    virtual void popNeighbor() = 0;
};

typedef std::shared_ptr<INeighborsContainer> neigh_cont_ptr;

}
