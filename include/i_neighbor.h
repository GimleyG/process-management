#pragma once

#include "i_entity.h"

namespace dyn_proc {

/**
* A base interaface for neighbor entity
*/
class INeighbor : public IEntity {
public:
    virtual ~INeighbor() {}

    virtual int getRank() = 0;
};

typedef std::shared_ptr<INeighbor> neigh_ptr;

}
