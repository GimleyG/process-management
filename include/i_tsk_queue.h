#pragma once

#include "base_proxy_ptr.h"
#include "i_job.h"

namespace dyn_proc {

/**
* A base interface for a task queue
*/
class ITaskQueue : public inheritable_enable_shared_from_this<ITaskQueue>{
public:
    virtual ~ITaskQueue() {}

    virtual job_ptr pop() = 0;
    virtual void push(const job_ptr &p) = 0;
    virtual bool empty() const = 0;
};

typedef std::shared_ptr<ITaskQueue> tsk_queue_ptr;

}
