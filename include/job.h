#pragma once

#include "i_job.h"
#include "types.h"

namespace dyn_proc {

/**
* A class of job's null-object
*/
class EmptyJob : public IJob {
public:
    virtual ~EmptyJob() {}

    bool isNone() { return true; }

    void perform() {
        throw std::logic_error("Nothing to do");
    }
    int getData() { throw std::logic_error("No data"); }
};

typedef std::shared_ptr<IJob> job_ptr;

}
