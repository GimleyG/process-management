#pragma once

#include "i_neighbor.h"

namespace dyn_proc {

/**
* A class that represents neighbor null-object
*/
class EmptyNeighbor : public INeighbor {
public:
    virtual ~EmptyNeighbor() {}
    bool isNone() { return true; }
    int getRank() { throw std::logic_error("Empty neighbor"); }
};


/**
* A neighbor realization
*/
class Neighbor : public INeighbor {
public:
    Neighbor(int rnk) : rank_(rnk) {}
    virtual ~Neighbor() {}

    bool isNone() { return false; }
    int getRank() { return rank_; }
private:
    int rank_;
};

}
