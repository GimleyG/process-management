#pragma once

#include "types.h"

namespace dyn_proc {

/**
* A structure to store all process information
*/
struct ProcInfo {
    int local_rank;
    int global_rank;
    MPI_Comm global_comm;
    neighbors_list_t neighbors;
};

}
