#pragma once

#include "mpi.h"

#include "i_comm_keeper.h"
#include "i_job.h"
#include "i_neigh_cont.h"
#include "neighbor.h"
#include "handler.h"
#include "task_queue.h"
#include "procinfo.h"
#include "cadger.h"

namespace dyn_proc {


/**
* A proxy class between task queue and cadger, worker, handler. Also controls a neighbors list access
*/
class QueueProxy : public ITaskQueue, public INeighborsContainer, public ICommKeeper {
public:
    virtual ~QueueProxy() = default;

    QueueProxy() = delete;
    QueueProxy(TaskQueue &tsks, MPI_Comm comm);

// ITaskQueue
    virtual job_ptr pop();
    virtual void push(const job_ptr &p);
    virtual bool empty() const;

// INeighborsContainer
    virtual std::shared_ptr<INeighbor> getNeighbor();
    virtual void popNeighbor();

// ICommKeeper
    virtual void changeComm(MPI_Comm comm);
private:    
    MPI_Comm comm_;
    neighbors_list_t neighbors_;

    TaskQueue &que_;


    void runCadger(CadgerRunMode rm);
};

}

