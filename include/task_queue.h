#pragma once

#include "i_tsk_queue.h"
#include "i_job.h"
#include "types.h"

namespace dyn_proc {

/**
* Simple ralization of a thread-safe queue
*/
class TaskQueue : public ITaskQueue {
public:
    TaskQueue() {}
    virtual ~TaskQueue() {}

    job_ptr pop();
    void push(const job_ptr &p);
    bool empty() const;

private:
    mutable mutex_t mutex_;
	tsk_queue_t     taskQueue_;
};

}
