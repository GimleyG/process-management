#pragma once

#include <list>
#include <memory>
#include <queue>
#include <stdexcept>

#include <condition_variable>
#include <mutex>
#include <thread>

namespace dyn_proc {

class IJob;
class INeighbor;

typedef std::queue<std::shared_ptr<IJob>> tsk_queue_t;
typedef std::list<std::shared_ptr<INeighbor>> neighbors_list_t;
typedef unsigned int tsk_weight_t;

typedef std::mutex               mutex_t;
typedef std::condition_variable  cond_var_t;
typedef std::thread              thread_t;

enum class Role {
    SERVER,
    CLIENT
};

}
