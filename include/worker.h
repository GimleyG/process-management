#pragma once

#include <memory>
#include "i_tsk_queue.h"

namespace dyn_proc {


/**
* A worker class that performs jobs and makes statistics of done jobs
*/
class Worker {
public:
    Worker() : finished_{false}, jobs_done_{0} {}
    Worker(tsk_queue_ptr &tsk_q) : finished_{false}, jobs_done_{0} {
        task_queue_ = tsk_q;
    }
    ~Worker() {}

    bool finished() const;
    void run() const ;

private:
     tsk_queue_ptr task_queue_;
     mutable bool finished_;

     mutable int jobs_done_;
};

typedef std::shared_ptr<Worker> worker_ptr;

}
