#include "cadger.h"

namespace dyn_proc {

void Cadger :: run(CadgerRunMode mode, MPI_Comm comm) {    
    int rank;
    MPI_Comm_rank(comm, &rank);

    std::cout << "cadger started in "<< rank << std::endl;
    while(true){
        auto neigh = neigh_cont_->getNeighbor();

        switch (mode) {
        /// asks neighbors for job and waits for response
        case CadgerRunMode :: DEFAULT:
            if(neigh->isNone()) {
                tsk_queue_->push(std::make_shared<EmptyJob>());
                return;
            }
            std::cout << "in " << rank << " ask " << neigh->getRank() << " for job" << std::endl;
            MPI_Send(&rank, 1, MPI_INT, neigh->getRank(), TAG_REQ, comm);

            int job;
            MPI_Recv(&job, 1, MPI_INT, neigh->getRank(), TAG_RESP, comm, MPI_STATUS_IGNORE);
            if(job == 0) {
                neigh_cont_->popNeighbor();
                continue;
            } else {
                job_ptr recv_job = createJob(job);
                tsk_queue_->push(std::move(recv_job));
                return;
            }
            break;
         /// sends stop signal to all neighbors
         case CadgerRunMode :: CHNG_COMM:
            if(neigh->isNone()) {
                return;
            }
            std::cout << "in " << rank << " send quit signal to " << neigh->getRank() << std::endl;
            MPI_Send(&rank, 1, MPI_INT, neigh->getRank(), TAG_STOP_CADGER, comm);
            neigh_cont_->popNeighbor();
            break;
        }
    }
}


}
