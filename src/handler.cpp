#include "handler.h"
#include <cassert>


namespace dyn_proc {

void Handler :: run() {
    std::cout << "Handler started " << std::endl;
    int rank;
    MPI_Comm_rank(comm_, &rank);
    finished_= false;
    int buf = 0;

    while(true) {
        std::unique_lock<std::mutex> lk(active_neigh_mtx_);
        if(active_neighbor_count_ <= 0)
            active_neigh_cond_.wait(lk, [this](){ return (active_neighbor_count_ > 0 || finished_); });
        lk.unlock();

        MPI_Status status;
        MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm_, &status);

        if(status.MPI_TAG == TAG_REQ) {
            MPI_Recv(&buf, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, comm_, MPI_STATUS_IGNORE);
            if(tsk_queue_->empty()) {
                auto empty_job = 0;
                MPI_Send(&empty_job, 1, MPI_UNSIGNED, status.MPI_SOURCE, TAG_RESP, comm_);

                std::unique_lock<std::mutex> lk(active_neigh_mtx_);
                active_neighbor_count_--;
                active_neigh_cond_.notify_all();

                std::cout << rank << ": Refuse in job to " << status.MPI_SOURCE << std::endl;
                continue;
            }
            auto tsk = tsk_queue_->pop();
            auto data = tsk->getData();
            MPI_Send(&data, 1, MPI_INT, status.MPI_SOURCE, TAG_RESP, comm_);
            std::cout << "process" << rank << " sent job to process " << status.MPI_SOURCE << std::endl;

        } else if( status.MPI_TAG == TAG_STOP_CADGER) {
            MPI_Recv(&buf, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, comm_, MPI_STATUS_IGNORE);

            std::unique_lock<std::mutex> lk(active_neigh_mtx_);
            active_neighbor_count_--;
            active_neigh_cond_.notify_all();

        } else if( status.MPI_TAG == TAG_TRM_HANDLER) {
            MPI_Recv(&buf, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, comm_, MPI_STATUS_IGNORE);
            break;

        } else {
            assert(false);
        }
    }
    std::cout << "Handler finished in " << rank << std::endl;
}

void Handler :: terminate() {
    int rank;
    MPI_Comm_rank(comm_, &rank);
    int buf = 0;
    std::unique_lock<std::mutex> lk(active_neigh_mtx_);
    if(active_neighbor_count_ > 0)
        active_neigh_cond_.wait(lk, [this](){ return active_neighbor_count_ <= 0; });
    lk.unlock();

    if(!finished_) {
        std::cout << "Finish handler "<< std::endl;
        finished_= true;
        active_neigh_cond_.notify_all();
        MPI_Send(&buf, 1, MPI_INT, rank, TAG_TRM_HANDLER, comm_);
    }
}

void Handler::changeComm(MPI_Comm comm) {
    int rank;
    MPI_Comm_rank(comm_, &rank);
    std::cout << "Change comm in handler " << rank << std::endl;
    std::unique_lock<std::mutex> lock(active_neigh_mtx_);
    active_neigh_cond_.wait(lock, [this](){ return active_neighbor_count_ <= 0; });

    comm_ = comm;
    MPI_Comm_size(comm_, &active_neighbor_count_);
    active_neighbor_count_--;
    active_neigh_cond_.notify_all();

    /// ждём, пока handler закончит принимать
    /// сообщения от всех cadger'ов
    /// после этого обновляем коммуникатор и количество активных соседей
    /// при такой тактике необходимо, чтобы handler получал оповещение позже cadger'a
}

}
