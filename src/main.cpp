#include <iostream>
#include <mpi.h>
#include <thread>

#include "connect_mng.h"
#include "i_job.h"
#include "neighbor.h"
#include "queue_proxy.h"
#include "worker.h"

#include "procinfo.h"

using namespace  dyn_proc;

int main(int argc, char **argv) {
    int provided;
    int proc_rank, proc_num;
    MPI_Comm glob_comm;
    dyn_proc::Role role;

    if(std::string(argv[1]) == "-s") {
        role = dyn_proc::Role::SERVER;
    } else if(std::string(argv[1]) == "-c") {
        role = dyn_proc::Role::CLIENT;
    } else {
        throw std::runtime_error("Invalid role for process set");
    }

    //======== MPI Init ===========
        MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
        if(provided != MPI_THREAD_MULTIPLE) {
         //   std::cout << "multiple thread is not provided" << std::endl;
            exit(1);
        }
        MPI_Comm_size(MPI_COMM_WORLD, &proc_num);
        MPI_Comm_dup(MPI_COMM_WORLD, &glob_comm);
        MPI_Comm_rank(glob_comm, &proc_rank);


    //======= Task & Queue init =======
        dyn_proc::TaskQueue task_queue;

        //tasks init
        for(int tsk_id = 0; tsk_id < 5; tsk_id++) {
            unsigned int wrk_time = abs(proc_rank + 1);
            task_queue.push(dyn_proc::createJob(wrk_time));
        }

    //======= Main entities =======
        std::shared_ptr<dyn_proc::base_proxy_ptr> queue_proxy(
                    std::make_shared<dyn_proc::QueueProxy>(task_queue, glob_comm)
        );
        auto queue_ptr = std::dynamic_pointer_cast<dyn_proc::ITaskQueue>(queue_proxy);
        auto worker    = std::make_shared<dyn_proc::Worker>(queue_ptr);
        auto handler   = std::make_shared<dyn_proc::Handler>(queue_ptr, glob_comm, proc_num - 1);

        std::thread handler_thread(&Handler::run, handler);
        std::thread worker_thread(&dyn_proc::Worker::run, worker);

    {
        ConnectManager mng(role, glob_comm);
        mng.addCommKeeper(std::dynamic_pointer_cast<ICommKeeper>(queue_ptr));
        mng.addCommKeeper(std::dynamic_pointer_cast<ICommKeeper>(handler));

        mng.start();

        worker_thread.join();
        handler->terminate();
        handler_thread.join();

        mng.stop();
    }

    MPI_Finalize();
    std::cout << "Finished!"<< std::endl;
}







