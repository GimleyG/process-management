#include <iostream>
#include <memory>


#include "neighbor.h"
#include "queue_proxy.h"

namespace dyn_proc {

QueueProxy :: QueueProxy(TaskQueue &tsks, MPI_Comm comm)
    : que_{tsks}, comm_{comm} {

    int proc_count, rank;
    MPI_Comm_size(comm_, &proc_count);
    MPI_Comm_rank(comm_, &rank);

    for(int i = 0; i < proc_count; i++) {
        if(rank == i) continue;
        auto neigh = std::make_shared<Neighbor>(i);
        neighbors_.push_back(std::move(neigh));
    }
}

// ==== ITaskQueue =====
job_ptr QueueProxy :: pop() {
    if(que_.empty()) {
        runCadger(CadgerRunMode :: DEFAULT);
      /// its supposed that the cadger will fill up the queue with some job, even with an empty
    }
   return que_.pop();
}

void  QueueProxy :: push(const job_ptr &p) {
    que_.push(p);
}

bool QueueProxy :: empty() const {
    return que_.empty();
}


// === INeighborsContainer ===
std::shared_ptr<INeighbor> QueueProxy :: getNeighbor() {
    if (neighbors_.empty()) {
        return std::make_shared<EmptyNeighbor>();
    }
    auto res = neighbors_.front();
    return res;
}

void QueueProxy :: popNeighbor() {
    neighbors_.pop_front();
}


// ===== ICommKeeper =====
void QueueProxy::changeComm(MPI_Comm comm){

    /// запускаем попрошайку в отдельном потоке в специальном режиме
    /// дожидаемся завершения и обновляем коммуникатор
    /// предполагается, что в ответ мы ничего не получим
    runCadger(CadgerRunMode :: CHNG_COMM);
    comm_ = comm;

    neighbors_.clear();

    int proc_count, rank;
    MPI_Comm_size(comm_, &proc_count);
    MPI_Comm_rank(comm_, &rank);

std::cout << "new size " << proc_count << std::endl;

    for(int i = 0; i < proc_count; i++) {
        if(rank == i) continue;
        auto neigh = std::make_shared<Neighbor>(i);
        neighbors_.push_back(std::move(neigh));
    }

}

void QueueProxy::runCadger(CadgerRunMode rm) {
    auto this_queue_ptr = ITaskQueue::downcasted_shared_from_this<QueueProxy>();
    auto this_neigh_ptr = INeighborsContainer::downcasted_shared_from_this<QueueProxy>();

    Cadger cdg(this_queue_ptr, this_neigh_ptr);
    std::thread cadger_thread(&Cadger::run, &cdg, rm, comm_);
    cadger_thread.join();
}


}
