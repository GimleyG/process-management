#include "task_queue.h"

namespace dyn_proc {

job_ptr TaskQueue :: pop() {
    std::lock_guard<std::mutex> lk(mutex_);
    job_ptr res = taskQueue_.front();
    taskQueue_.pop();
    return res;
}

void TaskQueue :: push(const job_ptr &new_val) {
    std::lock_guard<std::mutex> lg(mutex_);
    taskQueue_.push(new_val);
}

bool TaskQueue :: empty() const{
    std::lock_guard<std::mutex> lg(mutex_);
    return taskQueue_.empty();
}

}
