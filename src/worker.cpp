#include <iostream>
#include "worker.h"

namespace dyn_proc {

void Worker :: run() const {        
        while(true) {
            auto job = task_queue_->pop();
            if(job->isNone()) {
                 std::cout << "got empty job" << std::endl;
                break;
            } else {
                std::cout << "started job" << std::endl;
                job->perform();
                jobs_done_++;
            }
        }
        std::cout << "worker done " << jobs_done_ << " jobs" << std::endl;
    }

bool Worker :: finished() const {
    return finished_;
}

}
