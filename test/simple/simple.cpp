#include "i_job.h"
#include <thread>

using namespace dyn_proc;

class Job : public dyn_proc::IJob{
public:
    Job() { weight_ = 0; }
    Job(unsigned int w) : weight_(w) {}
    virtual ~Job() {}

    bool isNone() { return false; }

    void perform() {
        std::this_thread::sleep_for(std::chrono::seconds(weight_));
    }

    int getData() { return weight_;}
private:
    unsigned int  weight_;
};

dyn_proc::job_ptr dyn_proc::createJob(int weight) {
    return std::move(std::make_shared<Job>(weight));
}
